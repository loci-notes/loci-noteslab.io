---
title: Contributing
sidebar_position: 2
---

# Contributing
This page details how to properly contribute to all Loci Notes applications, both the server and related clients. Each repo might have it's own small changes, but in general this workflow should be followed for everything under [the Official Loci Notes Ecosystem](https://gitlab.com/loci-notes).

## Steps
1. Every change needs an issue. In general, the issues in each individual repo detail things that need to be done, but if a given change you want to make doesn't already exist, make one. Document the intended changes at a high leve, and make sure to apply the relevant label to the issue.
    * Enhancement - Nice to have feature, can be implemented later.
    * TODO - Needed feature, should be implemented soon.
    * Bug - Incorrect behavior, non security relevant.
    * Vulnerability - Incorrect behavior, security relevant.
    * Documentation - Improvements and fixes for repo docs.
    * Help - For when you need support, or something isn't working as you understand it should.
2. Once an issue is created, fork the repo or create a branch just for the issue. All changes related to the issue should be in this fork/branch.
3. Make your changes. Be sure to check linting requirements for any given application, or you can look at the CICD Pipeline checks to see what is required to pass.
4. Once ready, you can issue a Merge Request for your changes. In the merge request, make sure you refer to the issue number that your changes relate to ("Closes issue #<issue_num>"). Additionally, the merge request name itself should follow [the Conventional Commit format](https://www.conventionalcommits.org/en/v1.0.0/). Use "fix" for bug fixes, "feat" for features, "feat!" for breaking features, "chore" for minor linting fixes, etc. This is used in version calculation for releases, and all your commits will be squashed to this merge request message.
5. Wait for your changes to be merged! Releases will be issued periodically, but most will likely get released quickly.
