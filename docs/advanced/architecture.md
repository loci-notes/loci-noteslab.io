---
title: Architecture
sidebar_position: 2
---

# Architecture
This page will detail the design of the various components of Loci Notes, specifically in the backend. This is intended to be used by backend devs.