---
title: Introduction
sidebar_position: 2
---

## Overview
Loci Notes is a framework made up different applications and plugins that allows people doing security assessments to track notes and evidence in a way that is location-based, seamlessly integrated, and multi-user. This is a lot of buzzwords, so we'll break down each of these design goals.

### Location Based
This was the primary goal, hence the name "Loci". In this instance "location based" means that all notes should be attached the data/thought/object that originated them, instead of free-form. An example of free-form notes would be a flat text document that holds all of one person's (or more) thoughts about the project as a way to track what was done. By comparison, Loci Notes tracks all data entered by users as attached to known data points that can be translated according to the various Loci Notes clients. What this means is that if you're looking at `system_x`, Loci Notes should only show data and notes related to `system_x` in order to foster a better understanding of how it works, what has been done, what remains to be done, or any other information intended to be relayed by other users. Naturally this begs the question of "what does it mean to 'look at' `system_x`", which brings us to...

### Seamlessly Integrated
There are too many note-taking tools, security tools, and security note-taking tools. As a general rule, all of them require their own application, whether as a desktop app or as it's own web application. All data we wish to input (comments, notes, evidence, etc.) must be inserted into this separate tooling. This separation of information causes a rift between the tools we use to do the technical work, and the tools we use to read, store and process the extra information. Loci Notes intends to remove that separation by integrating into the technical tools already used via extension and plugin APIs offered by these tools. This information can be stored, retrieved, and processed directly without the need to keep entire separate toolchains just for notekeeping, eliminating the mental context switching that this involves. 

### Multi User
Any Loci Notes-enabled application should automatically handle the information to and from multiple people, across multiple workspaces, in as much real-time as possible. This will allow for more effective collaboration in instances where a project has multiple people working in the same area.

## Basic Concepts
In general, there are four objects users should understand in the LN framework; projects, users, artifacts, and notes.

### Projects
Projects are simply a way to group sets of related data. A single server can have multiple projects, and each project is entirely separate from all other projects (with some project grouping as a planned feature, not yet implemented). In general projects can have one or more "managers" (users who own the project), but otherwise simply hold the name of the project for easy identification. Any user can create a new (empty) project.

### Users
Users describe a single person with access to a given Loci Notes server. A user can have access to multiple projects (either that they created themselves, or that they were given access to), and tracks general information about the user so that other users can identify them. Each user can also be issued an API key for use in LN clients, to prevent password leakage.

### Artifacts
Artifacts make up the backbone of the LN system, and are the "locations" from which the "Loci" name is derived (the name itself was changed for a number of reason early in development). Put simply, artifacts are nothing more than strings attached to a singular project, with some metadata of their own attached. The meaning of the string (called a `descriptor`) is intended to be handled by the various LN clients, but there are a few rules defined about how to interpret descriptors so that clients can be (mostly) interoperable. For example, LN defines a specific point in code as being of the form:

`<repository>/<path>/<file_with_extension>:<line_number>`

An example of what this might look like is as follows:

* `repo-master/path/to/file.txt:4`
* `repo/path/to/other/file.json:10-20`
* `root.php:1`

This means that any LN client that can process artifacts of this form (like a text editor, for example) should be able to process this artifact to understand exactly what file it is referring to, and exactly what line(s), so that when the user is interested in that file and line, the information attached to the artifact is shown in a way that is understandable. How this information is shown and handled is the job of each client in order maximize information given to the user.

Other examples of artifact definitions:

* Hosts
    * `<ip address or hostname>`
    * Examples
        * `thetwitchy.com`
        * `127.0.0.1`
* Services
    * `<ip address or hostname>:<port>`
    * Examples
        * `thetwitchy.com:8080`
        * `127.0.0.1:21`
* URIs
    * `<scheme>://<authority>/<path>?<query>#<fragment>`
    * Examples
        * `http://thetwitchy.com/api/users`
        * `https://example.org/api?foo=some%20message`
* API Endpoints
    * `<http method> /<path>?<query>#<fragment>`
    * Examples
        * `GET /api/users`
        * `POST  /status?down=true`
* Generic
    * `<anything>`
    * Examples
        * `todos`
        * `literally anything in here that needs to be tracked manually`

:::caution

These definitions are *NOT* strictly enforced, as specific projects may dictate how artifacts need to be stored. For example, if an API has the form `GET /api/user/<id>`, ensuring that the artifacts generated from that API call are grouped together correctly is extremely important. Clients are responsible for configuring this correctly, and enforcing it upon users. At the end of the day, artifacts descriptors are nothing more than strings.

:::

In addition to the descriptors, each artifact also tracks it's own status. This allows users to track which artifacts need to be actively considered, and which ones do not. Loci Notes allows three different statuses:

* `TODO` - the default, intended to be used when an artifact needs to be handled normally
* `DONE` - used when the artifact has been handled and needs no further consideration (short of simply tracking previously entered data)
* `FLAG` - used when an artifact requires special handling, like in the event of a possible security issue

Artifacts are uniquely identified by their descriptors, so two artifacts in the same project cannot have the same descriptors.

### Notes
Lastly, notes are objects attached to artifacts. A single artifact can have multiple notes attached, and notes are only ever attached to a single artifact. Notes have a lot of metadata, but in general are represented by a string that describes the contents of the note, plus the type of the note. Currently supported note types include:

* Comments
    * General purpose comments that you would expect to be entered directly by users. This is the most common, and can be used to store contextual information about an artifact that may be helpful, or possible followup details, or anything of that nature.
* Links
    * These are links to other places, either artifacts or not. This can allow you to provide a way to quickly jump from one artifact to another in order to show the relation between them, or to show how some data flows from one to the other.
* Text Snapshot
    * This can be used to describe the artifact at a point in time. For example, if the artifact is a particular line of source code, the line itself could be stored in this note.
* PNG Snapshot
    * This can be used to describe the artifact in a picture. For example, if the artifact is a web service detected during a scan (like `http://example.com/admin`), a screenshot of the page could be stored here in PNG format.
* Log
    * This a type used to hold evidence about a particular artifact. For example, if the artifact is a network service, you may want to save the raw `nmap` output of the service for later consideration.

## Standard Setup
At it's most basic form, a Loci Notes setup involves two things:

* One server
* One or more clients

The server is the central store and arbiter of data, and simply enforces some rules about how to access the data (authentication and authorization), how data is stored, how data is retrieved, and some other basic tasks. In short, it's a very simple CRUD app. No task processing, no message queues, no cloud requirements, just a web application and an (optional) database. This allows it to be deployed anywhere in a way that is scalable, but still simple in the event that a special instance is required.

The client is the primary means by which users interact with Loci Notes, depending on the task, and the tool that Loci Notes is currently integrated into for said task. While the entire framework is still in development, and LN clients especially are still being planned, the following should give an idea of various clients and the tasks they can handle:

* General Purpose
    * [LN CLI](./clients/cli)
        * A basic command line interface to interact with LN.
        * This client supports multiple tasks and assessment types, and is intended to be a general purpose way to get raw data from the server.
        * It is not intended to be used as a day-to-day client, but supports most needed tasks.
    * LN OpenAPI Client
        * This is the auto-generated documentation available at `<your server address>/docs`. 
        * It describes the Loci Server API in detail, and can be used by users to make raw API calls, but in general is intended to be used by client developers.
* Source Code Review
    * [LN VS Code Extention](./clients/vs-code)
        * A client that integrated into the text editor [VS Code](https://code.visualstudio.com/) as an extension.
        * Generally creates and processes artifacts related to specific locations in a codebase.
        * Shows related information inline while viewing code and related files.
* Network Testing
    * LN nmap Processor
        * Parses `nmap` output and automatically attaches it to specific artifacts which represent the hosts and services scanned.
* Web Application Testing
    * Burp Extention
        * Will generate and process information related to web application testing, showing information inline with specific calls as represented by artifacts.