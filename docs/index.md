---
slug: /
title: Home
sidebar_position: 1
---

**This site is currently a work in progress. More complete documentation will be available soon.**

This is the official documentation site for Loci Notes, maintained by TheTwitchy.

Loci Notes is built for (and by) security professionals who need better tooling and workflows around code reviews, *especially* in very large and complex codebases. If you've ever been dropped into a new codebase and been told "find the security vulnerabilities, you have one week", then Loci Notes is for you. Even if you haven't, Loci Notes can probably help anyhow.

## At a Glance
* Integrated notes collection and management into standard developer tools like [VS Code](https://code.visualstudio.com/)
* Supports import of standard output from SAST tools like [semgrep](https://semgrep.dev/).
* Tracks status and priority of all notes across the codebase to prioritize areas likely to have issues.
* Integrated bookmark stack for code navigation.
* Fast export of search results to Loci Notes.
* Supports multiple simultaneous users for collaboration.

## Documentation
* Jump right into a step-by-step walkthrough of how to use Loci Notes in the [tutorial](./tutorial).
* Learn about the basics of Loci Notes in the [introduction](./intro).
* Users can reference client documentation in the [clients pages](./clients).
* For developers and the curious, the [advanced docs section](./advanced) has extra information about the inner workings of various components.
* Get help or tell the developer about all the bugs  Loci Notes via the [Discord channel](https://comingsoon.com)