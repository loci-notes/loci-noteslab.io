---
title: Basics
sidebar_position: 7
---
Now that we've imported the results of the Semgrep scan, lets review them directly inline with the possible vulnerable code. We'll also use some of the basic Loci Notes VS Code features to help us.

1. Return to VS Code and the Project Diligence project folder.
2. On the left sidebar, click the Loci Notes icon to bring up the Loci Notes pane. The pane has two sections, an Artifacts section, and a Navigation section. The Artifacts section will show all the locations in code Loci Notes knows about, and we can filter all the active code locations (or Artifacts) by status (`TODO`, `FLAG`, and `DONE`). There's also a priority attached to each Artifact to help prioritize which places we should look at first when we have a lot of results.
![vs-code-pane](/img/vs-code-pane.png)
3. We can also bring up a Comments pane to give a different view of the notes and results we have. If you don't see it, you may need to show it via View -> Output -> Comments
![vs-code-comments](/img/vs-code-comments.png)
4. From either the Navigation pane, or the Comments pane, click the first Artifact, which should take you to `diligence-main/src/main/java/com/thetwitchy/diligence/repositories/impl/DynUserRepositoryImpl.java:23`.
![vs-code-first-artifact](/img/vs-code-first-artifact.png)
5. Loci Notes has already captured the information about the result given to us by Semgrep. Use the information provided, and the references linked, to decide if this result is a False Positive (it was incorrectly marked as a problem), or a True Positive (it was correctly marked as a problem). Click the "Add a Note..." input field at the bottom, and add a sentence or two with your reasoning (with [Markdown](https://www.markdownguide.org/basic-syntax/) if you like). This will allow you to remember that this Semgrep result was handled, and what the outcome was during later references. You can quickly submit your comment with Ctrl-Enter.
![vs-code-comment](/img/vs-code-comment.png)
6. Update the status of this artifact using the Status buttons in the upper right corner of the comment pane. If this is a False Positive, you can mark it as `DONE`, and if it is a True Positive, mark it as `FLAG` to show that we should prioritize it later. You can also change the Priority of the artifact in the same pane, if you feel it is necesary.
![vs-code-comment-buttons](/img/vs-code-comment-buttons.png)
7. Last, lets update the left Artifacts pane to show the updated status for everything. Click the Refresh button in the upper right hand corner of the pane, and you should see the artifact we just handled move out of the `TODO` list.
![vs-code-done](/img/vs-code-done.png)