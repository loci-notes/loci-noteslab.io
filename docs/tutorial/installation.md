---
title: Server Installation
sidebar_position: 3
---
In the following tutorial, we'll install the Loci Notes server so that you (and possibly others) can reach it.

1. Open a terminal on your server host.
2. Ensure Docker and Docker Compose are installed correctly:
```bash
docker version
docker compose version
```
3. Download the latest source code from https://gitlab.com/loci-notes/loci-server/-/releases. You want the "Loci Notes Server Build Files"
```bash
curl -O {URL for loci-server-build.tgz}
tar xzvf loci-server-build.tgz
```
4. Enter the `loci-server-build` directory.
```bash
cd loci-server-main-build/
```
5. Copy over the ENV file.
```bash
cp .env_EXAMPLE .env
```
6. Open `.env` and set the values appropriately. At a minimum, you should need to set:
* `DOMAIN` - You Loci Notes Server domain name, like `loci-notes.yourdomain.com`
* `BACKEND_CORS_ORIGINS` - Add your domain from above.
* `SECRET_KEY` - Any long and random string.
* `FIRST_SUPERUSER` - Email address of the server admin.
* `FIRST_SUPERUSER_PASSWORD` - Password of the server admin. Any long and random string.
* `POSTGRES_PASSWORD` - Any long and random string.
* `STORAGE_SECRET_KEY` - Any long and random string.
* `SMTP_*` - SMTP values, if email is needed (optional)
7. Edit the `Caddyfile` to reflect your domain with port 443. (if needed)
8. Bring up the Docker Compose stack.
```bash
docker compose up -d
```