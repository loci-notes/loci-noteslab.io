---
title: Project Setup
sidebar_position: 4
---
Now that the Loci Notes server is setup, we need a project to try it out. For this tutorial we'll use [Project Diligence](https://gitlab.com/TheTwitchy/diligence), a vulnerable-by-design application designed to help practice code navigation.

All remaining steps take place on your normal work machine, *not* the Loci Notes server.

1. Install the [Loci Notes CLI tooling](https://gitlab.com/loci-notes/loci-cli) onto your working machine.
```bash
pip3 install loci-cli
```
2. Go to the local folder where you'll store all work related to review of Project Diligence. We'll use `~/projects/diligence` for this tutorial, but  put this wherever makes sense for your work.
```bash
mkdir -p ~/projects/diligence 
cd ~/projects/diligence
```
3. Login to the Loci Notes instance. This will setup your local machine to connect to the Loci Notes server.
```bash
loci login
```

```bash
[INFO] Logging in to Loci Notes server at https://loci-notes.yourdomain.com as you@gmail.com...
[SUCCESS] Login successful!
[INFO] Creating and saving a new API key user@host...
[SUCCESS] API key created and saved to /home/user/.config/loci/loci-config.ini
```
4. Setup your project folder.
```bash
loci setup
```

```bash
Server [https://loci-notes.yourdomain.com]: 
Project name [diligence]: 
[INFO] The project diligence could not be found...
Create the project diligence? [y/N]: y
[SUCCESS] Project diligence created!
[SUCCESS] Project diligence saved to /home/user/projects/diligence.
[SUCCESS] Loci Notes source directory /home/user/projects/diligence/_src created.
[INFO] Store your Loci Notes source files in this directory. See the README for more information.
```
5. Download the [Project Diligence source code ZIP](https://gitlab.com/TheTwitchy/diligence) (Code -> Download Source Code -> ZIP).
6. Put the ZIP file into the project folder. This is not strictly necesary, but makes things easier as the project associated with the uploaded code is autodetected because it is in the correct folder. If you want to upload from somewhere else, just make sure to specify the server location and project name during the `upload` call later.
```bash
mv ~/Downloads/diligence-main.zip .
```
7. Upload the source code to the project.
```bash
loci upload diligence-main.zip
```
8. Sync your local project folder.
```bash
loci sync
```
9. Verify that all the source code was downloaded from the server correctly into your local `_src` folder (this is where Loci Notes stores a local copy of all code files). You can use the `tree` command as shown below as well.
```bash
user@host ~/projects/diligence
 # tree -a -L 3
.
├── diligence-main.zip
├── .loci-project.ini
└── _src
    ├── diligence-main
    │   ├── build.gradle
    │   ├── .gitignore
    │   ├── gradle
    │   ├── gradlew
    │   ├── LICENSE
    │   ├── Project Diligence.postman_collection.json
    │   ├── README.md
    │   ├── settings.gradle
    │   └── src
    └── README-LOCI.md
```

> Although it may seem odd to upload files to the server, and then download them again when you could just unzip the files locally, the reason we do it this way is because when multiple users are working on the same project, we need *perfect* consistency across all local copies of the source code across all user's machines. This workflow ensures that all users will have the exact same directory structure for all code files, even if it's changed later.
