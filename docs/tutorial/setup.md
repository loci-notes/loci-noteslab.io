---
title: Server Setup
sidebar_position: 2
---
Loci Notes requires a server in order to store all notes, which must be hosted by the user (in other words, there is no public Loci Notes server provided).

To host a server you will need:
* A host for the Loci Notes server. This guide assume a *nix host, but instructions can be trivially modified for Windows or WSL.
    * A t3-micro-sized instance is fine for single users, and t3-medium+ for multiple users, generally. This is just a rough guideline, and you can use *any* *nix-based server, VPS, or computer to host your Loci Notes server.
* A working machine with [Python 3](https://www.python.org/) and [VS Code](https://code.visualstudio.com/) installed.
    * This *can* be the same computer as where you run the server, but this is generally a desktop or laptop where you perform most work (not a server).
* [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) installed on the server host.
    * [Docker Desktop](https://docs.docker.com/desktop/) is an option for running it on your local machine, but will not allow multiple users to collaborate.
* Optionally, a domain name pointing at the server host above. This is highly recommended for obtaining a TLS certificate, and is used for secure connections to the Loci Notes server.