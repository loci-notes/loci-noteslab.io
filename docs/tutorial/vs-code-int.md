---
title: Intermediate
sidebar_position: 8
---
We've removed one of the possible vulnerabilties found by Semgrep, and you've likely noticed another very similar Semgrep result directly under it. Lets do the same review on this next possible vulnerability.

1. In VS Code, go to the next Semgrep result at `diligence-main/src/main/java/com/thetwitchy/diligence/repositories/impl/DynUserRepositoryImpl.java:30`. Determine if this result is a False Positive or True Positive.
2. Based on the information given, and the context where it was found, this Artifact is a True Positive, but we don't yet know if this is exploitable. In other words, without any other context in the application, this is an unsafe way to build a SQL query, but we need to see if the input parameter `String username` can be maliciously influenced by user input. In [more formal terms](https://blog.shiftleft.io/how-to-review-code-for-vulnerabilities-1d017c21a695), the Semgrep result represents a Sink, and we need to find a Source that connects to it. VS Code provides a number of tools out of the box to help us find a Source, and we can enhance these capabilities with features provided by Loci Notes.
3. First, we need to find out what calls the `findByUsernameContaining3()` function, and there are generally two ways to do this, both of which we'll explore here.
4. First, we'll perform a simple search across all files in the VS Code Workspace to see if there are any other references to the string `findByUsernameContaining3`, as these are *probably* a call to the function we care about (but not always, be careful!). Do this with [VS Code's built in Search functionality](https://code.visualstudio.com/docs/editor/codebasics#_search-across-files) (Ctrl-Shift-F is the hotkey). You can limit the search scope to just the `_src/` folder if you like. After removing the function itself, and the function definition in the interface, you should be left with a call from `diligence-main/src/main/java/com/thetwitchy/diligence/services/impl/UserServiceImpl.java:52`.

![vs-code-search](/img/vs-code-search.png)

5. A second way to find the same information is to use [Code Navigation features provided by VS Code](https://code.visualstudio.com/docs/editor/editingevolved), especially for [getting references to symbols](https://code.visualstudio.com/docs/editor/editingevolved#_reference-information). This works differently to the raw search above, in that it interprets the code according the the language, and can give you a true sense of what is calling a function, instead of just simple string matching. Go back to `diligence-main/src/main/java/com/thetwitchy/diligence/repositories/impl/DynUserRepositoryImpl.java:30` (a trick detailed [here](https://code.visualstudio.com/docs/editor/editingevolved#_quick-file-navigation) is to paste the entire location, including the line number, into the Quick Open window opened with Ctrl-P, or you can just put your curosr over the `findByUsernameContaining3` function call and press F12), and get a list of all references to that function, either with Find All References (Alt-Shift-F12), or via right-click -> Peek -> References.

![vs-code-refs](/img/vs-code-refs.png)

> Using raw string searches vs using code-aware reference lookups for navigation is generally done on a case-by-case basis, and there are pros and cons to each. Being able to do both on the fly is incredibly helpful when dealing with a complex codebase.

6. Regardless of which way you choose, we should see that the `findByUsernameContaining3` function is only ever called by the function `getUsers3` in the `UserServiceImpl` class. Lets drop a comment here to make sure we know why this particular call is important. If we mouse over the right-hand side of the line numberin the editor, we get a "+" button that we can use to insert a comment. One interesting thing to note here is that we can actually insert links to other artifacts within our comment, to make navigation easier in the future. To get the Markdown link to our vulnerable sink, go back to the `DynUserRepositoryImpl` and click the "Copy Artifact Markdown Link" in the upper right corner.

![vs-code-comment-link](/img/vs-code-comment-link.png)

> You can also get a Markdown/raw link for any section of code by right clicking inside the editor and selecting "Copy Artifact Location" or "Copy Artifact Markdown Link".

7. We now have a small set of linked Artifacts in code to help us navigate around in relation to this possible SQL injection issue. While the two Artifacts we have isn't hard to manage, this can quickly become a complex set of multiple linking possible issues with hundreds or thousands of Artifact nodes to manage. While linking Artifacts manually can be helpful, there are more powerful ways to help us keep track of where we are, where we've been, and where we need to go inside the code.
