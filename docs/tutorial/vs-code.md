---
title: VS Code Setup
sidebar_position: 5
---
The vast majority of work with Loci Notes is done in VS Code, to give a better view of the code that you'll review. In this tutorial we'll install it into your VS Code instance and make sure it's detecting the project you created earlier.

1. Start VS Code and open the project folder from earlier.
```bash
code ~/projects/diligence
```
2. If you haven't already installed it, install the [Loci Notes VS Code Extension](https://marketplace.visualstudio.com/items?itemName=TheTwitchy.loci-notes-vs-code).
3. Restart VS Code (if needed) and open the project folder again.
4. You should immediately receive an informational pop-up from Loci Notes giving the project name, your name, and confirmation that everything is installed correctly.

![popup](/img/vs-code-popup.png)

5. Briefly browse through the source code files in the `_src` folder to take a first look around in VS Code. See if you can figure out what language Project Diligence is written in, what build system it uses to define the frameworks it uses, and what major frameworks it relies on as dependencies. A search engine is your friend here. Spend about 10 minutes here, you don't need to understand every file at this time.
