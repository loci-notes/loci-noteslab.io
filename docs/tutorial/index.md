---
title: Tutorial
sidebar_position: 1
---

This tutorial will walk you through the full installation process, and then the basic of how to use Loci Notes against a vulnerable-by-design codebase.

1. [Setup](./setup)
2. [Server Installation](./installation)
3. [Project Setup](./project-setup)
4. [VS Code Setup](./vs-code)
5. [Semgrep](./semgrep)
6. [Basics](./vs-code-basics)
7. [Intermediate](./vs-code-int.md)
8. [Advanced](./vs-code-adv.md)