---
title: Semgrep
sidebar_position: 6
---
The first thing we should do when reviewing code for security vulnerabilties is to run a SAST tool like [Semgrep](https://semgrep.dev/) to see if there is anything obviously wrong. Loci Notes supports Semgrep integration out of the box, and we can import results directly into Loci Notes. This means that instead of looking at the results of a scan in a terminal, we can view them inline next to the code that triggered them to get a better view of the context for each result.

1. Install the Semgrep CLI, according to [the official documentation](https://semgrep.dev/docs/getting-started/cli).
2. From your project folder, run Semgrep with default rulesets against the `_src` folder where all the source code is contained.
```bash
semgrep scan _src
```

```bash
                              
┌──── ○○○ ────┐
│ Semgrep CLI │               
└─────────────┘               

Scanning 22 files (only git-tracked) with:

...snip...                                                                                                                         
                   
┌─────────────────┐
│ 3 Code Findings │
└─────────────────┘
                                                                                                                        
                                                                                                            
  _src/diligence-main/src/main/java/com/thetwitchy/diligence/repositories/impl/DynUserRepositoryImpl.java            
    ❯❱ java.lang.security.audit.sqli.jpa-sqli.jpa-sqli
          Detected a formatted string in a SQL statement. This could lead to SQL injection if variables in the
          SQL statement are not properly sanitized. Use a prepared statements (java.sql.PreparedStatement)    
          instead. You can obtain a PreparedStatement using 'connection.prepareStatement'.                    
          Details: https://sg.run/DoOd                                                                        
                                                                                                              
           23┆ List<User> results = entityManager.createQuery(query, User.class).getResultList();
   
   ❯❯❱ java.lang.security.audit.formatted-sql-string.formatted-sql-string
          Detected a formatted string in a SQL statement. This could lead to SQL injection if variables in the
          SQL statement are not properly sanitized. Use a prepared statements (java.sql.PreparedStatement)    
          instead. You can obtain a PreparedStatement using 'connection.prepareStatement'.                    
          Details: https://sg.run/OPXp                                                                        
                                                                                                              
           30┆ return entityManager.createQuery(query, User.class).getResultList();
   
    ❯❱ java.lang.security.audit.sqli.jpa-sqli.jpa-sqli
          Detected a formatted string in a SQL statement. This could lead to SQL injection if variables in the
          SQL statement are not properly sanitized. Use a prepared statements (java.sql.PreparedStatement)    
          instead. You can obtain a PreparedStatement using 'connection.prepareStatement'.                    
          Details: https://sg.run/DoOd                                                                        
                                                                                                              
           30┆ return entityManager.createQuery(query, User.class).getResultList();
                
                
┌──────────────┐
│ Scan Summary │
└──────────────┘
Some files were skipped or only partially analyzed.
  Scan skipped: 1 files matching .semgrepignore patterns
  For a full list of skipped files, run semgrep with the --verbose flag.

Ran 157 rules on 21 files: 3 findings.
💎 Missed out on 852 pro rules since you aren't logged in!
⚡ Supercharge Semgrep OSS when you create a free account at https://sg.run/rules.
```

3. Before we import the results, look through the issues that Semgrep found to see if any stick out.
4. We want to import the results of the scan into Loci Notes, but we need something that Loci Notes can understand, not just terminal output. Do the scan again, but this time export the results to a JSON file.
```bash
semgrep scan _src --json -o semgrep.json
```

```bash
                              
┌──── ○○○ ────┐
│ Semgrep CLI │               
└─────────────┘               
                              
Scanning 22 files (only git-tracked) with:
                                      
...snip...                                                                                                                     
                
┌──────────────┐
│ Scan Summary │
└──────────────┘
Some files were skipped or only partially analyzed.
  Scan skipped: 1 files matching .semgrepignore patterns
  For a full list of skipped files, run semgrep with the --verbose flag.

Ran 157 rules on 21 files: 3 findings.
💎 Missed out on 852 pro rules since you aren't logged in!
⚡ Supercharge Semgrep OSS when you create a free account at https://sg.run/rules.

```
4. Import the results stored in the `semgrep.json` file into Loci Notes, using the `loci` CLI tool. You can add the `--dry-run` option to see what will be imported first, and the CLI version has some filters available if you only want to import a subset of results.
```bash
loci semgrep semgrep.json
```

```bash
[INFO] Processing semgrep.json...
[WARNING] Semgrep JSON file version 1.68.0 is not officially supported. There may be some errors in how this file is processed.
Processing Semgrep results... ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 100% 0:00:02
[INFO] Results Summary:
[INFO]     HIGH: 2
[INFO]     MEDIUM: 1
[INFO]     LOW: 0
[SUCCESS] Semgrep results processed successfully!
```
