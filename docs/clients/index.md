---
title: Clients
sidebar_position: 1
---

# Clients - Introduction
This is the official user documentation site for various Loci Notes clients. For developer-specific information, see the README in each client repository.

* [Command Line Interface](./cli)
* [VS Code Extension](./vs-code)