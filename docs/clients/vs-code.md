---
title: Loci Notes VS Code Extension
sidebar_position: 3
---

The official docs for the Loci Notes VS Code Client.

## Source
https://gitlab.com/loci-notes/loci-vs-code

## Installation
[Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=TheTwitchy.loci-notes-vs-code)
