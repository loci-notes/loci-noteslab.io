---
title: Loci Notes CLI
sidebar_position: 2
---

# Loci Notes CLI
This is the official documentation for the Loci Notes command line client (CLI).

## Source
https://gitlab.com/loci-notes/loci-cli

## Installation
The CLI supports Python3+, and is available on the [PyPI](https://pypi.org/project/loci-cli/).

`pip3 install loci-cli`
