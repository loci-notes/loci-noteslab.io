---
slug: /faqs
title: Frequently Asked Questions
sidebar_position: 10
---
This page keeps track of frequently (and not-so-frequently) ask questions.

## Who built Loci Notes?
A: [TheTwitchy](https://gitlab.com/TheTwitchy) is the maintainer and primary developer of Loci Notes. Also, the primary user.

## What's the level of overall stability?
A: Generally stable enough that's is used nearly daily in a professional context for code reviews, but there' are probably going to be minor problems and bugs here and there. In general, if you're doing code reviews proessionally, it shouldn't be anything you can't handle, and I try to fix things as I can given they tend to annoy me too.

## Why don't you provide a public server?
A: Because hosting costs are expensive, and I don't want your data. You also don't want me to have your data given it probably details security problems.

## Is "Loci" pronounced Low-key or Low-kai?
Yes.

## What's up with the raven and the magnifying glass?
![meme](/img/meme.jpg)
