import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Track notes, evidence, and status of assessment tasks',
    description: (
      <>
        Never lose track of notes or evidence again, and always know what remains to be done.
      </>
    ),
  },
  {
    title: 'Integrate into existing workflows and tools',
    description: (
      <>
        Integrates into tools you already use, so no more switching to another page or application.
      </>
    ),
  },
  {
    title: 'Collaborate with others automatically',
    description: (
      <>
        Automatically collaborate with others, getting information exactly where it matters.
      </>
    ),
  },
];

function Feature({title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
